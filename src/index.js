const express = require('express');
const gpt = require('./routes/gpt_routes.js');
const app = express();
app.use(express.json());
app.use('/v1/gpt', gpt);
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`servidor corriendo en el puerto ${PORT}`);
});