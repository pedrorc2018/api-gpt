
require('dotenv').config();
const axios = require('axios');
class GptControllers {
    async consultOpenAI (req, res) {
        const question = req.body.question;
        const url = 'https://api.openai.com/v1/chat/completions';
        try {
            const apiKey = process.env.OPENAI_API_KEY;
            const response = await axios.post(url, {
                model: 'gpt-4-0125-preview',
                messages: [
                    {
                        role: 'system',
                        content: 'You are a helpful assistant that provides answers related to digital marketing.'
                    },
                    {
                        role: 'user',
                        content: question
                    }
                ]
            }, {
                headers: {
                    Authorization: `Bearer ${apiKey}`
                }
            });
            res.send({
                status: true,
                data: response.data.choices[0].message.content.trim(),
            })
        } catch (error) {
            throw error;
        }
    }

}
module.exports = GptControllers