const express = require('express');
const router = express.Router();
const GptControllers = require('../controllers/gpt_controllers');
const gpt = new GptControllers()

router.post('/question', gpt.consultOpenAI)
module.exports = router;